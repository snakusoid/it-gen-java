package academy.prog.lesson1;

import java.util.Scanner;

/**
 * My First Java Program
 * 
 * @author snakusoid
 * @version 0.1
 */

public class MyFirstClass {
    public static void main(String[] args) {
        // It's a code that allows you to enter your name and then displays it.
        Scanner sc = new Scanner(System.in);
        System.out.println("Kudenko Ihor");
        String name = sc.nextLine();
        System.out.println(name);
    }
}
