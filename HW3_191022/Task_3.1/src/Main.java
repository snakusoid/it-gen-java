/**
 * @author Ihor Kudenko
 */
//Program made for arithmetic operations
public class Main {
    public static void main(String[] args) {
        short a = 1988; //Year of birth
        short b = 34; //Result of multiplication of numbers related to Day and Month of birth
        short c = (short) (Math.pow((a + b), 2) / ((Math.pow(a, 2)) + (3 * (Math.pow(b, 2)))));//(a + b)^2 / (a^2 + 3b^2)
        System.out.println("Result of (a + b)^2 / (a^2 + 3b^2) arithmetic expression: " + c);
        System.out.println("\n");
        /*
        Означення проміжних змінних для чисельника і знаменника
         */
        int num = (int) Math.pow((a + b), 2); //Numerator (a + b)^2
        int den = (int) ((int) Math.pow(a, 2) + 3 * Math.pow(b, 2)); //Denominator (a^2 + 3b^2)
        byte resOfDivision = (byte) (num / den);
        System.out.println("Numerator (a + b)^2 = " + num);
        System.out.println("Denominator (a^2 + 3b^2) = " + den);
        System.out.println("Result of division operation = " + resOfDivision);
        System.out.println("\n");
        /*
        Проміжні змінні типу float
         */
        float numF = (float) Math.pow((a + b), 2); //Numerator (a + b)^2
        float denF = (float) ((float) Math.pow(a, 2) + 3 * Math.pow(b, 2)); //Denominator (a^2 + 3b^2)
        float resOfDivisionF = numF / denF;
        System.out.println("Numerator (a + b)^2 in 'float' = " + numF);
        System.out.println("Denominator (a^2 + 3b^2) in 'float' = " + denF);
        System.out.println("Result of division operation in 'float' = " + resOfDivisionF);
        System.out.println("\n");
        /*
        Проміжні змінні типу 'double'
         */
        double numD = Math.pow((a + b), 2); //Numerator (a + b)^2
        double denD = Math.pow(a, 2) + 3 * Math.pow(b, 2); //Denominator (a^2 + 3b^2)
        double resOfDivisionD = numD / denD;
        System.out.println("Numerator (a + b)^2 in 'double' = " + numD);
        System.out.println("Denominator (a^2 + 3b^2) in 'double' = " + denD);
        System.out.println("Result of division operation in 'double' = " + resOfDivisionD);
        /*
        Завданням було написати программу для обчислення виразу (a + b)^2 / (a^2 + 3b^2).
        Для піднесення чисел до степеня було використано метод Math.pow(). Так як він
        автоматично присвоює введенним даним тип 'double', то для виведення результату
        через типи даних з меншим об'ємом пам'яті застосовувалася примусова конвертація.
        Результати першого і другого обчислення однакові, бо типи даних 'short' та 'int'
        вміщують в собі тільки цілі числа розміром до 16 та 32 біт відповідно.
        Третє обчислення з даними типу 'float' дало результат з дробовим числом з сіма
        знаками після коми.
        Четверте - в результаті мало число з шістнадцятьма знаками після коми, бо для
        обчислення використовувалися дані типу 'double'.
         */
    }
}