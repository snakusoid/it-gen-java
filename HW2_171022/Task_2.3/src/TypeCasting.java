/**
 * @author Ihor Kudenko
 */
//Program made for explicit type casting
public class TypeCasting {
    public static void main(String[] args) {
        short birthdaySum = 2007; // Number we get after addition of Day, Month and Year of Birth numbers.
        byte castToByte = (byte) birthdaySum; //Explicit type conversion from 'short' to 'byte'.
        char castToChar = (char) birthdaySum; //Explicit type conversion from 'short' to 'char'.
        System.out.println("Set value of 'short' type data: "
                + birthdaySum +
                "\n Its binary form: " + Integer.toBinaryString(birthdaySum));
        System.out.println("Result of explicit type casting of data from 'short' to 'byte': "
                + castToByte +
                "\n Its binary form: " + Integer.toBinaryString(castToByte));
        System.out.println("Result of explicit type casting of data from 'short' to 'char': "
                + castToChar + " 'Nko Letter Cha (U+07D7)' " +
                "\n Its binary form: " + Integer.toBinaryString(castToChar));
/*
Опис результатів:
Була написана програма для примусового перетворення даних типу 'short' в інший.
Перший метод виконує примусову конвертацію даних типу 'short' (16 біт) до 'byte' (8 біт).
При цьому заданним числом втрачаються старші вісім бітів, які не вміщуються в тип даних 'byte'
і в результаті ми отримуємо число -41.
Це видно по бінарним формам обох чисел:
2007 = 00000111 11010111;
-41 = 11010111; (функція Integer.toBinaryString будь-яке заданне число перетворює на тип 'int',
тому на екран результат виводиться в 32-х бітах.
У випадку з конвертацією з типу 'short' до типу 'char' кількість бітів залишається незмінною, бо
обидва типи даних вміщують 16 бітів, проте відбувається зміна типу інформації з числової на буквенну.
 */
    }
}