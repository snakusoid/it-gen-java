import java.util.Scanner;

/**
 * @author Ihor Kudenko
 */

public class IntersectionPointCalculationIfElse {
    public static void main(String[] args) {
        double a1 = 2; //slope
        double a2 = -3; //slope
        double b1 = 5; //y-intercept
        double b2 = 7; //y-intercept
        /*
        Line 1: 2x + 5
        Line 2: -3x + 7
         */
        if (a1 == a2) {
            System.out.println("Lines are parallel and don't intersect.");
            return;
        } else {
            System.out.println("Calculating intersection point...");
        }
        double x = (b2 - b1) / (a1 - a2);
        double y = a1 * x + b1;
        System.out.println("x: " + x + "\ny: " + y);

        Scanner xyIn = new Scanner(System.in);
        System.out.println("Next we are going to add another point and compare its position to intersection point coordinates.");
        System.out.println("Please, enter new coordinates:");
        System.out.println("X:");
        double xIn = Double.parseDouble(xyIn.nextLine());
        System.out.println("Y:");
        double yIn = Double.parseDouble(xyIn.nextLine());
        if (yIn == y & xIn == x) {
            System.out.println("Given coordinates are the same as calculated intersection point.");
        } else if (yIn < y & xIn == x) {
            System.out.println("Given coordinates are below calculated intersection point.");
        } else if (yIn > y & xIn == x) {
            System.out.println("Given coordinates are above calculated intersection point.");
        } else if (yIn == y & xIn < x) {
            System.out.println("Given coordinates are on the left side from intersection point");
        } else if (yIn > y & xIn < x) {
            System.out.println("Given coordinates are on the upper left side from intersection point");
        } else if (yIn < y & xIn < x) {
            System.out.println("Given coordinates are on the lower left side from intersection point");
        } else if (yIn == y & xIn > x) {
            System.out.println("Given coordinates are on the right side from intersection point");
        } else if (yIn > y & xIn > x) {
            System.out.println("Given coordinates are on the upper right side from intersection point");
        } else if (yIn < y & xIn > x) {
            System.out.println("Given coordinates are on the lower right side from intersection point");
        }
    }
}