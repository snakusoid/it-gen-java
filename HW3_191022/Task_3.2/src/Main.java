/**
 * @author Ihor Kudenko
 */
/*
Program created for calculation of different variables using bitwise and ternary operators.
 */
public class Main {
    public static void main(String[] args) {
        int d = 17 % 16;
        int m = 2;
        int a = d | m; //OR
        int b = a ^ d >>> 3; //XOR
        int c = a & ~(d << 2); //AND NOT
        int e = (a == b) ? 0 : 5; //Ternary operator
        System.out.println("Remainder of 17 by 16 division operation assigned to variable 'd': " + (d)
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(d & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(d & 0xf));
        System.out.println("Number assigned to variable 'm': " + m
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(m & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(m & 0xf));
        System.out.println("Result of bit-by-bit operation done by bitwise operator OR " +
                "(for 'd' and 'm') assigned to variable 'a': " + a
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(a & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(a & 0xf));
        System.out.println("Result of bit-by-bit operation done by bitwise operator XOR " +
                "(for 'a' and 'd >>> 3') assigned to variable 'b': " + b
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(b & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(b & 0xf));
        System.out.println("Result of bit-by-bit operation done by bitwise operator AND " +
                "(for 'a' and '~(d << 2)') assigned to variable 'c': " + c
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(c & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(c & 0xf));
        System.out.println("Result of operation done by ternary operator, " +
                "where it checks the statement that 'a' equals 'b'. True returns '0' and false returns '5'. " +
                "Assigned to variable 'e': " + e
                + "\nBinary format with 0xf mask: " + Integer.toBinaryString(e & 0xf)
                + "\nHexadecimal format with 0xf mask: " + Integer.toHexString(e & 0xf));
    }
}