/**
 * @author Ihor Kudenko
 *
 */
public class Primitives {
    public static void main(String[] args) {
        byte typeByte = 122; //byte primitive data type
        short typeShort = -9999; //short primitive data type
        int typeInt = 1234567890; //int primitive data type
        long typeLong = -543211234511111L; //long primitive data type
        float typeFloat = 3.14f; //float primitive data type
        double typeDouble = 17.22211111111; //double primitive data type
        boolean typeBoolean = true; //boolean primitive data type
        char typeChar = 'A'; //char primitive data type
        //Printing different primitive data types
        System.out.println("byte = " + typeByte);
        System.out.println("short = " + typeShort);
        System.out.println("int = " + typeInt);
        System.out.println("long = " + typeLong);
        System.out.println("float = " + typeFloat);
        System.out.println("double = " + typeDouble);
        System.out.println("boolean = " + typeBoolean);
        System.out.println("char = " + typeChar);
    }
}