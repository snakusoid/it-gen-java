/**
 * @author Ihor Kudenko
 */
//Завдання 4.2:
//Вивести на екран дані у заданому вигляді.
public class Main {
    public static void main(String[] args) {
        for (int i = 1; i < 9; i++) {
            for (int j = i; j > 0; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}