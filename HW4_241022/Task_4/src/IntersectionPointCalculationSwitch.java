import java.util.Scanner;

/**
 * @author Ihor Kudenko
 */

public class IntersectionPointCalculationSwitch {
    public static void main(String[] args) {
        double a1 = 2; //slope
        double a2 = -3; //slope
        double b1 = 5; //y-intercept
        double b2 = 7; //y-intercept
        /*
        Line 1: 2x + 5
        Line 2: -3x + 7
         */
        if (a1 == a2) {
            System.out.println("Lines are parallel and don't intersect.");
            return;
        } else {
            System.out.println("Calculating intersection point...");
        }
        double x = (b2 - b1) / (a1 - a2);
        double y = a1 * x + b1;
        System.out.println("x: " + x + "\ny: " + y);

        Scanner xyIn = new Scanner(System.in);
        System.out.println("Next we are going to add another point and compare its position to intersection point coordinates.");
        System.out.println("Please, enter new coordinates:");
        System.out.println("X:");
        double xIn = Double.parseDouble(xyIn.nextLine());
        System.out.println("Y:");
        double yIn = Double.parseDouble(xyIn.nextLine());
        String yAxisCheck = yIn > y ? "Upper" : yIn < y ? "Lower" : "Same";
        String xAxisCheck = xIn < x ? "Left" : xIn > x ? "Right" : "Same";
        String coordinatesPos;
                /*
                Виконання задачі з умовним оператором Switch.
                 */
//                switch (yAxisCheck + xAxisCheck){
//                    case "Upper" + "Left": coordinatesPos = "Given coordinates are on the upper left side from intersection point";
//                    break;
//                    case "Upper" + "Right": coordinatesPos = "Given coordinates are on the upper right side from intersection point";
//                    break;
//                    case "Upper" + "Same": coordinatesPos = "Given coordinates are above calculated intersection point.";
//                    break;
//                    case "Lower" + "Left": coordinatesPos = "Given coordinates are on the lower left side from intersection point";
//                    break;
//                    case "Lower" + "Right": coordinatesPos = "Given coordinates are on the lower right side from intersection point";
//                    break;
//                    case "Lower" + "Same": coordinatesPos = "Given coordinates are below calculated intersection point.";
//                    break;
//                    case "Same" + "Left": coordinatesPos = "Given coordinates are on the left side from intersection point";
//                    break;
//                    case "Same" + "Right": coordinatesPos = "Given coordinates are on the right side from intersection point";
//                    break;
//                    default: coordinatesPos = "Given coordinates are the same as calculated intersection point.";
//                }
                    /*
                    Виконання завдання з умовним оператором Enhanced switch.
                     */
        coordinatesPos = switch (yAxisCheck + xAxisCheck) {
            case "Upper" + "Left" -> "Given coordinates are on the upper left side from intersection point";
            case "Upper" + "Right" -> "Given coordinates are on the upper right side from intersection point";
            case "Upper" + "Same" -> "Given coordinates are above calculated intersection point.";
            case "Lower" + "Left" -> "Given coordinates are on the lower left side from intersection point";
            case "Lower" + "Right" -> "Given coordinates are on the lower right side from intersection point";
            case "Lower" + "Same" -> "Given coordinates are below calculated intersection point.";
            case "Same" + "Left" -> "Given coordinates are on the left side from intersection point";
            case "Same" + "Right" -> "Given coordinates are on the right side from intersection point";
            default -> "Given coordinates are the same as calculated intersection point.";
        };
        System.out.println(coordinatesPos);
    }
}
